<?php
/**
 * User: 万奇
 * Date: 2021/9/3 17:37
 */

namespace app\admin\controller\v1;

use think\App;
use app\core\BaseController;
use think\facade\Cache;
use think\facade\Db;
use think\Request;


class Login extends BaseController
{
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * 登录
     * User: 万奇
     * Date: 2021/9/3 17:41
     * @throws \think\db\exception\DbException
     */
    public function login(){
        $param          = param_receive(['phone', 'password']);
        $where          = ['phone' => $param['phone']];
        $user           = Db::name('sys_user')->where($where)->find();
        if (!$user){
            response(500, '用户不存在');
        }

        if ($user['disable'] == 1){
            response(500, '该账号已被禁用');
        }

        $result = \app\admin\model\SysUser::encryption($user['id'], $user['password'], $param['password']);
        if ($result != false) {
            Cache::set('user_info_' . $user['id'], $user);
            response(200, '登录成功', ['user_id' => $user['id'], 'token' => $result, 'phone' => $user['phone'], 'is_main' => $user['is_main']]);
        }

        response(500, '密码错误');
    }


    /**
     * 微信登录
     * User: Shy
     * Date: 2021/9/3 17:45
     * @param Request $request
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function WxLogin(Request $request){
        $config     = Db::name('wxk_config')->where(true)->find();
        if ($request->isPost()) {
            if (!$config['wxk_id'] || !$config['wxk_app_agent_id']){
                response(500, '未配置应用信息');
            }
            //获取登录url
            $url = Config('common.wx_login_url');
            $redirect_url = $request->domain() . '/login';
            $url .= "appid={$config['wxk_id']}&agentid={$config['wxk_app_agent_id']}&redirect_uri=$redirect_url&state=STATE";
            response(200, '成功', ['url' => $url]);
        } else {
            //回调 获取access_token
            $url = Config('common.wx_access_token');
            $url .= "corpid={$config['wxk_id']}&corpsecret={$config['wxk_app_secret']}";
            $result = httpGet($url);
            $result = json_decode($result, true);
            $access_token = $result['access_token'];
            //获取微信user_id
            $url1 = Config('common.wx_get_user_info');
            $code = $request->data['code'];
            $url1 .= "access_token=$access_token&code=$code";
            $result1 = httpGet($url1);
            $result1 = json_decode($result1, true);
            if ($result1['errcode'] == 0) {
                $staff = \app\admin\model\WxkStaff::where(['user_id' => $result1['UserId']])->find();
                $user = \app\admin\model\SysUser::where(['phone' => $staff->mobile])->find();
                if ($staff && $user) {
                    $user->token = \app\admin\model\SysUser::token();
                    $user->last_login_at = date('Y-m-d H:i:s', time());
                    $user->save();
                    Cache::set($user->id,['phone'=> $user->phone]);
                    response(200, '登录成功', ['user_id' => $user->id, 'token' => $user->token, 'phone' => $user->phone]);
                }else{
                    response(500, '请先联系管理员注册');
                }
            }
            response(500, '失败');
        }
    }

}