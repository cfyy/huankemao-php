<?php
/**
 * Created by Shy
 * Date 2020/12/1
 * Time 14:22
 */


namespace app\admin\controller\v1;


use think\App;

class SysUser extends BasicController
{
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * 子账户重置密码
     * User: 万奇
     * Date: 2021/2/6 0006
     */
    public function user_reset_pwd(){
        param_receive(['id', 'pwd', 'verify_pwd']);
        $model          = new \app\admin\model\SysUser();
        $model->user_reset_pwd($this->param);

        response(200, '操作成功');
    }

    /**
     * 子账户列表
     * User: 万奇
     * Date: 2021/2/5 0005
     */
    public function get_user_list(){
        param_receive(['page', 'limit']);
        $model          = new \app\admin\model\SysUser();
        $result         = $model->get_user_list($this->param);

        response(200, '', $result['data'], $result['total']);
    }

    /**
     * 新增子账户
     * User: 万奇
     * Date: 2021/2/5 0005
     */
    public function user_add(){
        param_receive(['username', 'phone', 'gender', 'department', 'password', 'role_id']);

        $this->validate($this->param, ['username' => 'max:50', 'phone' => 'unique:sys_user|regex:/^1[3-9]\d{9}$/', 'password' => 'min:6'],
            ['username.max' => '用户名限50字符', 'phone.unique' => '手机号已存在', 'phone.regex' => '手机号格式错误', 'password.min' => '密码不小于6字符']);

        $model          = new \app\admin\model\SysUser();
        $model->user_add($this->param, $this->user_info);

        response(200, '操作成功');
    }

    /**
     * 批量修改子账户状态
     * User: 万奇
     * Date: 2021/2/6 0006
     */
    public function user_edit_disable(){
        param_receive(['id', 'disable']);
        $model          = new \app\admin\model\SysUser();
        $model->user_edit_disable($this->param);

        response(200, '操作成功');
    }

    /**
     * 编辑子账户
     * User: 万奇
     * Date: 2021/2/5 0005
     */
    public function user_edit(){
        param_receive(['id', 'username', 'gender', 'department', 'role_id']);

        $this->validate($this->param, ['username' => 'max:50'], ['username.max' => '用户名限50字符']);

        $model          = new \app\admin\model\SysUser();
        $model->user_add($this->param, $this->user_info);

        response(200, '操作成功');
    }

    /**
     * 获取子账户 信息
     * User: 万奇
     * Date: 2021/2/5 0005
     * @throws \think\db\exception\DbException
     */
    public function get_user_info(){
        param_receive(['id']);

        $model          = new \app\admin\model\SysUser();
        $result         = $model->get_user_info($this->param);

        response(200, '', $result);
    }

    /**
     * 退出登录
     * User: 万奇
     * Date: 2021/2/7 0007
     */
    public function logout(){
        $model          = new \app\admin\model\SysUser();
        $model->logout($this->user_info);

        response(200, '退出成功');
    }

//    /**
//     * 发送验证码
//     * User: 万奇
//     * Date: 2020/9/9 0009
//     * @throws \AlibabaCloud\Client\Exception\ClientException
//     */
//    public function get_sms_code(){
//        $param              = param_receive([ 'phone']);
//        $code               = rand(100000,999999);
//        $this->redis->set('sms_code_'. $param['phone'] , $code , 300); // 缓存验证码 有效期 5 分钟
//
//        $aliyun_sms         = new AliyunSms();
//        $sms                = $aliyun_sms->send_sms($param['phone'], $code, 1);
//
//        response($sms['code'], $sms['msg']);
//    }

//    /**
//     * 修改个人密码
//     * User: 万奇
//     * Date: 2021/2/6 0006
//     */
//    public function edit_user_pwd(){
//        $param              = param_receive(['id', 'pwd', 'verify_pwd', 'code', 'phone']);
//
//        if ($param['code']  != $this->redis->get('sms_code_'. $param['phone'])){
//            response(500, '验证码错误');
//        }
//
//        $this->redis->delete('sms_code_'. $param['phone']);
//
//        $model          = new \app\admin\model\SysUser();
//        $model->user_reset_pwd($this->param);
//
//        response(200, '操作成功');
//    }


}