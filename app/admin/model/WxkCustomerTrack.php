<?php
/**
 * Created by Shy
 * Date 2020/12/24
 * Time 17:20
 */


namespace app\admin\model;

class WxkCustomerTrack extends BasicModel
{
    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }

    /**
     * 客户互动轨迹列表
     * User: 万奇
     * Date: 2021/7/6 17:36
     * @param $param
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function get_customer_track_list($param){
        $where[]        = ['external_user_id', '=', $param['external_user_id']];
        return $this->where($where)->order(['create_at' => 'desc'])->paginate($param['limit'])->toArray();
    }

    /**
     * 添加互动轨迹
     * User: 万奇
     * Date: 2021/7/6 17:36
     * @param $external_user_id
     * @param $content
     * @param $type
     */
    static public function add_customer_track($external_user_id, $content, $type){
        self::insert(['id' => uuid(), 'external_user_id' => $external_user_id, 'content' => $content, 'type' => $type]);
    }

}