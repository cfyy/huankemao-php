<?php
/**
 * Created by Shy
 * Date 2020/12/3
 * Time 15:19
 */


namespace app\admin\model;

class SysModule extends BasicModel
{
    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }

    /**
     * 权限设置列表
     * User: 万奇
     * Date: 2021/2/5 0005
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function show_roles_module(){
        $list_all = array_grouping($this->field('id,code,title,parent_code,url,is_menu,icon,disable')->where(['disable' => 0])->order('sort','asc')->select()->toArray(), 'parent_code');

        if (!$list_all){
            response(200, '', $list_all);
        }

        // 通过一级目录树状结构查询(无限级)
        return category_group($list_all, $list_all[0], 'group', 'code');
    }

}