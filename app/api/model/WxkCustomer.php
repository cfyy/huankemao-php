<?php
/**
 * User: 万奇
 * Date: 2021/7/21 17:16
 */

namespace app\api\model;


use think\facade\Db;

class WxkCustomer extends BasicModel
{
    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }

    /**
     * 首页概览
     * User: 万奇
     * Date: 2021/7/23 10:35
     * @param $param
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function home_overview($param){
        // 客户总计
        $result['customer']       = Db::name('enter_customer')->where(['follow_userid' => $param['uid']])->count();
        $result['wx_customer']    = Db::name('wxk_customer')->where(['follow_userid' => $param['uid']])->count();
        $result['all_count']      = $result['customer'] + $result['wx_customer'];

        // 今日客户数量变动
        $today_count        = Db::name('wxk_live_qr_statistics')->field('COUNT(DISTINCT case when add_customer = 1 then external_user_id end) day_add_customer,
        COUNT(DISTINCT case when add_customer = 0 then external_user_id end) deleted_customer')
        ->whereDay('create_at')->where(['user_id' => $param['uid']])->select()->toArray()[0];

//        // 超时未跟进
//        $result['overtime']       = 0;
//        $rule                     = Db::name('customer_recovery_rule')->value('remind');
//        if ($rule){
//            $remind_date              = date('Y-m-d', strtotime("-{$rule} day"));
//            $remind_where             = "corp_id = '{$param['corp_id']}' and follow_userid = '{$param['uid']}' and follow_time < '{$remind_date}'";
//            $result['overtime']       = Db::name('wxk_customer')->where($remind_where)->count();
//        }

        // 未跟进
        $result['not_follow']     = Db::name('wxk_customer')->where(['follow_status' => 1])->count();

        // 拓客计划进度统计
//        $wxk_cu_model             = new \app\admin\model\WxkCustomer();
//        $date_customer            = $wxk_cu_model->_develop_custom_progress(['corp_id' => $param['corp_id'], 'staff_id' => $param['uid']]);

        // 柱形图
        $data       = ['index' => 1, 'date_type' => 1, 'start_time' => $param['start_time'], 'end_time' => $param['end_time'], 'page' => 1, 'limit' => 7];
        $stat_model = new \app\admin\model\WxkLiveQrStatistics();
        $screen     = $stat_model->get_live_qr_stat_screen($data, false);

        return ['count' => $result, 'today_count' => $today_count, 'screen' => $screen['data']];
    }



}