<?php
/**
 * Created by PhpStorm.
 * User: 万奇
 * Date: 2020/11/19 0019
 * Time: 18:22
 */

use think\facade\Route;

// 规则：（路由名禁止重复） Route::post('api_test', 'api/Test/test'); 访问方式域名admin/'.$v.'.api_test；

// 测试
Route::post('index' , 'api/Index/index');

// 企业微信 JS-SDK
Route::post('get_js_sdk' , 'api/CorpWeChat/get_js_sdk');

// 登录通过code获取成员信息
Route::post('get_user_info' , 'api/Login/get_user_info');

// 首页概览
Route::post('home_overview' , 'api/Index/home_overview');

// 公共OSS上传
Route::post('upload_file_oss' , 'api/Index/upload_file_oss');

// 客户列表
Route::post('get_list_customer' , 'api/WxkCustomer/get_list_customer');

// 非企微客户列表
Route::post('get_enter_customer_list' , 'api/EnterCustomer/get_enter_customer_list');

// 转交非企微客户
Route::post('shift_enter_customer' , 'api/EnterCustomer/shift_enter_customer');

// 非企微客户退回公海
Route::post('back_pool_enter_customer' , 'api/EnterCustomer/back_pool_enter_customer');

// 回显新增编辑非企微客户数据
Route::post('show_edit_enter_customer' , 'api/EnterCustomer/show_edit_enter_customer');

// 添加非企微客户
Route::post('add_enter_customer' , 'api/EnterCustomer/add_enter_customer');

// 非企微客户池列表
Route::post('get_customer_pool_list' , 'api/CustomerPool/get_customer_pool_list');

// 客户池添加客户
Route::post('add_customer_pool' , 'api/CustomerPool/add_customer_pool');

// 认领客户池客户
Route::post('claim_customer_pool' , 'api/CustomerPool/claim_customer_pool');

// 非企微客户详情
Route::post('enter_customer_info' , 'api/EnterCustomer/enter_customer_info');

// 非企微客户打标签/移除标签
Route::post('enter_customer_tagging' , 'api/EnterCustomer/enter_customer_tagging');

// 非企微客户跟进记录列表
Route::post('enter_customer_follow_list' , 'api/EnterCustomer/enter_customer_follow_list');

// 添加非企微客户跟进
Route::post('add_enter_customer_follow' , 'api/EnterCustomer/add_enter_customer_follow');

// 获取非企微客户画像
Route::post('enter_customer_portrait' , 'api/EnterCustomer/enter_customer_portrait');

// 修改非企微客户画像
Route::post('edit_enter_customer_info' , 'api/EnterCustomer/edit_enter_customer_info');

// 非企微客户互动轨迹列表
Route::post('enter_customer_track_list' , 'api/EnterCustomer/enter_customer_track_list');

// 获取选择成员列表
Route::post('get_show_staff_list' , 'api/EnterCustomer/get_show_staff_list');

// 获客统计
Route::post('get_customer_statistic' , 'api/Encourage/get_customer_statistic');

// 激励详情
Route::post('get_encourage_list' , 'api/Encourage/get_encourage_list');

// 客户详情
Route::post('get_customer_info' , 'api/WxkCustomer/get_customer_info');

// 修改客户详情
Route::post('edit_customer_info' , 'api/WxkCustomer/edit_customer_info');

// 获取客户画像
Route::post('get_customer_portrait' , 'api/WxkCustomer/get_customer_portrait');

// 回显编辑客户画像
Route::post('show_edit_customer_portrait' , 'api/WxkCustomer/show_edit_customer_portrait');

// 上传客户画像
Route::post('upload_customer_portrait_photo' , 'api/WxkCustomer/upload_customer_portrait_photo');

// 添加客户跟进
Route::post('add_customer_follow' , 'api/WxkCustomer/add_customer_follow');

// 客户跟进记录列表
Route::post('get_customer_follow_list' , 'api/WxkCustomer/get_customer_follow_list');

// 获取客户标签树结构
Route::post('get_customer_tag_tree' , 'api/WxkCustomer/get_customer_tag_tree');

// 客户打标签
Route::post('customer_tagging' , 'api/WxkCustomer/customer_tagging');

// 客户互动轨迹列表
Route::post('get_customer_track_list' , 'api/WxkCustomer/get_customer_track_list');


