<?php
/**
 * Created by PhpStorm.
 * User: 万奇
 * Date: 2020/11/19 0019
 * Time: 18:02
 */

namespace app\api\controller;


use app\core\BaseController;
use think\App;
use think\facade\Db;

class BasicController extends BaseController
{
    protected $param; // 保存请求的参数

    protected $user_info; // 用户信息

    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->set_user_info(); // 处理参数和用户信息
        $this->_user_auth(); // 用户鉴权
    }

    /**
     * 设置获取用户相关的参数
     * User: 万奇
     * Date: 2020/9/3 0003
     * @param $param
     * @return array
     */
    private function get_auth($param){
        $url                = $this->request->url();
        $auth               = isset($this->request->header()['auth']) ? $this->request->header()['auth'] : response(101, '请登录');
        $auth               = param_receive(['uid', 'time', 'sign'], '', json_decode($auth, true));

//        if ((time() - $auth['time']) > 60){
//            response(500, 'sign time 超时');
//        }
//
//        $sign               = md5($auth['time'] . md5('huankemao666' . $url . ($param ? str_replace('+', '%20', http_build_query($this->request->param(false))) : "{}")));
//        if ($sign != $auth['sign']) {
//            response(500, '传输协议错误');
//        }

        return $auth;
    }

    /**
     * 设置用户信息
     * User: 万奇
     * Date: 2020/9/3 0003
     * @param array $handle_param 需要处理的参数
     */
    protected function set_user_info($handle_param = []){
        $this->param        = input('post.');
        $this->user_info    = $this->get_auth($this->param);
        $this->param['uid']            = $this->user_info['uid'];
    }

    /**
     * 用户鉴权
     * User: 万奇
     * Date: 2020/9/3 0003
     */
    protected function _user_auth(){
        $user       = Db::name('wxk_staff')->where(['user_id' => $this->user_info['uid']])->count();

        if (!$user){
            response(500, '用户不存在');
        }
    }

}