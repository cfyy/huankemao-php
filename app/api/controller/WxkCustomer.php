<?php
/**
 * 客户模块
 * Created by PhpStorm.
 * User: 万奇
 * Date: 2021/3/26 0026
 * Time: 10:30
 */

namespace app\api\controller;


use think\App;

class WxkCustomer extends BasicController
{
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * 客户打标签
     * User: 万奇
     * Date: 2021/1/15 0015
     */
    public function customer_tagging(){
        param_receive(['id', 'tag_ids', 'type']);
        $customer = new \app\admin\model\WxkCustomer();
        $customer->customer_tagging($this->param, $this->user_info['uid'], 2);

        response(200, '操作成功');
    }

    /**
     * 获取客户标签树结构
     * User: 万奇
     * Date: 2020/12/8 0008
     */
    public function get_customer_tag_tree(){
        $model      = new \app\admin\model\WxkCustomerTag();
        $result     = $model->get_customer_tag_tree();

        response(200, '', $result);
    }

    /**
     * 客户互动轨迹列表
     * User: 万奇
     * Date: 2021/7/6 17:36
     * @throws \think\db\exception\DbException
     */
    public function get_customer_track_list(){
        param_receive(['external_user_id', 'page', 'limit']);

        $customer = new \app\admin\model\WxkCustomerTrack();
        $result = $customer->get_customer_track_list($this->param);

        response(200, '', $result['data'], $result['total']);
    }

    /**
     * 客户跟进记录列表
     * User: 万奇
     * Date: 2021/3/25 0025
     * @throws \think\db\exception\DbException
     */
    public function get_customer_follow_list(){
        param_receive(['external_user_id', 'follow_userid', 'page', 'limit']);

        $customer = new \app\admin\model\WxkCustomerFollow();
        $result = $customer->get_customer_follow_list($this->param);

        response(200, '', $result['data'], $result['total']);
    }

    /**
     * 添加客户跟进
     * User: 万奇
     * Date: 2021/3/23 0023
     * @throws \think\db\exception\DbException
     */
    public function add_customer_follow(){
        param_receive(['external_user_id', 'follow_userid', 'add_follow_user', 'follow_type', 'content']);
        $customer = new \app\admin\model\WxkCustomerFollow();
        $customer->add_customer_follow($this->param, 2);

        response(200, '操作成功');
    }

    /**
     * 回显编辑客户画像
     * User: 万奇
     * Date: 2021/3/24 0024
     */
    public function show_edit_customer_portrait(){
        $customer = new \app\admin\model\WxkCustomer();
        $result = $customer->show_edit_customer_portrait();

        response(200, '', $result);
    }

    /**
     * 获取客户画像
     * User: 万奇
     * Date: 2021/3/23 0023
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function get_customer_portrait(){
        param_receive(['external_user_id']);
        $customer = new \app\admin\model\WxkCustomer();
        $result = $customer->get_customer_portrait($this->param);

        response(200, '', $result);
    }

    /**
     * 修改客户详情
     * User: 万奇
     * Date: 2021/3/23 0023
     * @throws \think\db\exception\DbException
     */
    public function edit_customer_info(){
        param_receive(['follow_userid', 'external_user_id']);
        $customer = new \app\admin\model\WxkCustomer();
        $customer->edit_customer_info($this->param, $this->user_info['uid'], 2);

        response(200, '操作成功');
    }

    /**
     * 获取客户详情
     * User: 万奇
     * Date: 2021/3/23 0023
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function get_customer_info(){
        param_receive(['follow_userid', 'external_user_id', 'is_api']);

        $customer = new \app\admin\model\WxkCustomer();
        $result = $customer->get_customer_info($this->param);

        response(200, '', $result);
    }

    /**
     * 客户列表
     * User: 万奇
     * Date: 2020/12/3 0003
     * @throws \think\db\exception\DbException
     */
    public function get_list_customer(){
        param_receive(['page', 'limit', 'follow_userid']);

        $customer = new \app\admin\model\WxkCustomer();
        $result = $customer->get_list_customer($this->param);

        response(200, '', $result['data'], $result['count']);
    }

}