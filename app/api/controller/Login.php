<?php
/**
 * Created by PhpStorm.
 * User: 万奇
 * Date: 2021/2/4 0004
 * Time: 11:31
 */

namespace app\api\controller;

use app\core\BaseController;
use think\App;

class Login extends BaseController
{
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * 通过code获取成员信息
     * User: 万奇
     * Date: 2021/7/30 18:42
     * @throws \think\db\exception\DbException
     */
    public function get_user_info(){
        $param      = param_receive(['code']);

        $model      = new \app\api\model\WxkStaff();
        $result     = $model->get_user_info($param);

        response(200, '', $result);
    }

}