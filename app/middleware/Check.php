<?php
declare (strict_types=1);

namespace app\middleware;

use Closure;
use Exception;
use think\facade\Cache;
use think\facade\Db;
use think\Request;

class Check
{

    protected $data;

    /**
     * 处理跨域问题
     */
    public function __construct(){
        // 制定允许其他域名访问
        header("Access-Control-Allow-Origin:*");//*代表所有域名访问，多个域名以逗号分隔
        // 响应类型
        header('Access-Control-Allow-Methods:POST,GET');
        // 响应头设置
        header('Access-Control-Allow-Headers:auth, x-requested-with, content-type');
    }

    /**
     * 处理请求
     * User:Shy
     * @param Request $request
     * @param Closure $next
     * @return mixed|\think\response\Json
     * @throws \think\db\exception\DbException
     */
    public function handle($request, Closure $next)
    {
        $this->data = $request->request();
        $request->data = $this->data;

        // 安装文件生成后，忽略权限列表
        $ignoreList = ['login', 'wx_login', 'logout', 'send_msg', 'register', 'verify_code', 'check_environment', 'install','install_check','admin/forget_pas',
            'forget_pas','content_type_list','config_synchro', 'del_config', 'get_temporary_preview', 'wechat_login_test', 'wechat_login', 'add_article_reading',
            'get_code_staff_user', 'app_get_content_engine', 'get_js_sdk', 'content_operating', 'get_menu_list'];

        // 安装文件生成前，外网回调路由地址,直接忽略
        $external_route     = ['external_contact', 'get_config_random_string'];
        if (in_array(request()->pathinfo(), $external_route)){
            return $next($request);
        }

        $url = str_replace('/admin.php/','',\think\facade\Request::url());//路由
        if($request->isGet()){
            return $next($request);
        }
        try {
            $content = file_get_contents('../install/install.lock');
        } catch (Exception $e) {
            $content = false;
        }
        if ($url == 'check_environment' || $url == 'install') {
            if ($content != false) {
                return rsp(500, '已安装');
            }

        }else{
            if ($content === false && $url != 'install_check' ) {
                return rsp(505, '未安装');
            }
        }
        if (in_array($url, $ignoreList)) {
            return $next($request);
        } else {
            // 检测是否配置企业
            if (!in_array(request()->pathinfo(), ['add_wxk_config', 'wxk_config_list', 'get_callback_url', 'upload_domain_verification_file'])){
                $wxk_id     = Db::name('wxk_config')->where(true)->value('wxk_id');
                if (!$wxk_id){
                    return rsp(500, '当前未配置企业微信信息，请前往系统设置页面配置');
                }
            }

            return $next($request);
        }
    }


}
